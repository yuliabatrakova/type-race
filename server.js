const path = require('path')
const express = require('express')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const passport = require('passport')
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser')
const users = require('./users')
let gameStatus = false

require("./passport.config.js")
app.use(express.static(path.join(__dirname, '/public')))
app.use(passport.initialize());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

server.listen(3000)
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, "index.html"));
});

app.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, "login.html"));
});

app.get('/game', function (req, res) {
    res.sendFile(path.join(__dirname, "game.html"));
});
app.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
        const token = jwt.sign(userFromReq, "someSecret", { expiresIn: '24h' });
        res.status(200).json({ auth: true, token });
    } else {
        res.status(401).json({ auth: false })
    }
});
    io.on('connection', function(socket) {
        socket.emit('someone-new-connected')
        // socket.emit('start', { gameStatus: false })

    //     socket.on('start-room', payload => {
    //     socket.join(payload.room);
    // })
        socket.on('chekingUser', ({ token }) => {
            const user = jwt.verify(token, 'secret');
            if (!user) {
                socket.emit('leaveTheGame')
            }
        })
        // socket.on('start-room', () => {

        // })
    })