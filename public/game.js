window.onload = () => {

    let randomNumber = Math.floor(Math.random() * 4)
    const texts = [
    "The central problem of human beings isn't religion, as the New Atheists insist. It's tribalism. We know this in part because chimps, our closest biological kin, go to war, and they are not religious, although they are tribal. Tribalism also has a central problem — and it's not competition, despite the tendency of competition to produce, at least temporarily, winners and losers. it's cooperation, because cooperation is what allows us to exist as bounded groups.",
    "People must unite under the banner, not of their group, and not of nothingness, but of the individual. This is a brilliant and intrinsically paradoxical solution to the problems of nihilistic nothingness and too-rigid group identity alike. It is the consciousness of the individual which transforms the chaos of potential into habitable cosmos, as the greatest origin stories repeatedly insist. It is that same consciousness which stands up, rebellious and revelatory.",
    "It is that consciousness, not the objective material substrate of Being, which should be regarded as the ultimate reality. There is no self-evident reason why dead matter should be given ontological primacy over living spirit. Although doing so has produced a massive increase in human technological power, it has left that power in hands of an increasingly disenchanted populace, and that presents a mortal danger. Such power must be wielded by those who have truly and voluntarily accepted the responsibility of Being.",
    "The West has long been the civilised embodiment of the idea of the divine individual, who does exactly that. That’s what the voluntarily lifting of the cross of suffering symbolically represents. For all its faults, which are manifold, the West has therefore served as a shining beacon of hope to those destined to inhabit places too chaotic or too rigid for the human spirit to tolerate. But the West is in grave danger of losing its way. The negative consequences of this can hardly be overstated."
]
    const jwt = localStorage.getItem('jwt')
    if(!jwt) {
        location.replace('/login')
    } else {
        const textToType = document.querySelector('#text-to-type')
        textToType.textContent = texts[randomNumber]
        const typingArea = document.querySelector('#typing-area')
        const timerBeforeStart = document.querySelector('#timerBeforeStart')
        const timer = document.querySelector('#timer')
        const time = document.querySelector('#time')
        const timeOverMessage = "Time is up"
        const progressOne = document.getElementsByClassName('player-progress')[0]
        const firstPlayerName = document.getElementsByClassName('player-name')[0]
        const progressTwo = document.getElementsByClassName('player-progress')[1]
        let isPlaying = false
        const winner = document.querySelector('#winner')
        const winnerMessage = document.querySelector('#results-table')
        const input = typingArea.value
        const errorSound = document.querySelector('#audio')

        const socket = io.connect("http://192.168.1.72:3000/")

            socket.on('someone-new-connected', () => {
                textToType.classList.add('visible')
                timer.classList.add('visible')
                startTimer(10, timerBeforeStart)
                socket.emit('start-room')
            })

        //     socket.on('start', payload => {
        //         if (!payload.gameStatus) {
        //             const token = localStorage.getItem('jwt')
        //             socket.emit('gettingRoom', {room: 'race'})
        //             fetch('/game', {
        //             method: "POST",
        //             headers: {
        //                 'Content-Type': 'application/json',
        //                 Authorization: `Bearer ${token}`
        //             }
        //     }).then(res => {
        //         res.json().then(body => {
        //             socket.emit('someone-connected', {token: token});
        //             textToType.classList.add('visible')
        //             timer.classList.add('visible')
        //             startTimer(2, timerBeforeStart)
        //         })
        //     })
        // }   if (payload.gameStatus) {
        //         socket.emit('gettingRoom', {room: 'rest'});
        //         // textToType.classList.add('hidden')
        //     }
        // })

            socket.on('leaveTheGame', () => {
                location.reload('/login')
            })
            let trimmedText = textToType.textContent.trim()
            console.log(textToType.textContent)
            typingArea.oninput = function() {
            const input = typingArea.value
            let startBool = trimmedText.startsWith(input)
            if(startBool === true) {
                let matchingText = trimmedText.substring(0, input.length)
                let progress = matchingText.length / trimmedText.length * 100
                progressOne.value = progress
                textToType.innerHTML = textToType.textContent.replace(matchingText, 
                `<span class='green'>${matchingText}</span>`)
                if(progressOne.value === 100 || progressTwo.value === 100) {
                    progressOne.value === 100 ? showWinner(firstPlayerName.innerText) : 
                    showWinner(progressTwo.value)           }
            } else {
                errorSound.play()
            }
        }       
        console.log(progressOne.value)
        function showWinner(player) {
            winner.innerText = player
            winnerMessage.classList.add('visible')
        }
        function startTimer(duration, display) {
            if(display === timerBeforeStart) {
                isPlaying = false 
                let divInfo = document.createElement('div')
                divInfo.innerText = "time before start"
                timerBeforeStart.appendChild(divInfo)
            }
        let timing = duration, minutes, seconds
        const func = setInterval(function () {
            minutes = parseInt(timing / 60, 10)
            seconds = parseInt(timing % 60, 10)

            minutes = minutes < 10 ? "0" + minutes : minutes
            seconds = seconds < 10 ? "0" + seconds : seconds

            display.textContent = minutes + "m : " + seconds + "s"
            if (--timing < 0) {
                if(display === timer) {
                    display.textContent = timeOverMessage
                    typingArea.readOnly = "true"
                    isPlaying = false
                    if(progressOne.value > progressTwo.value || progressTwo.value > progressOne.value) {
                    progressOne.value > progressTwo.value ? showWinner(firstPlayerName.innerText) : 
                    showWinner(progressTwo.value)
                }
                }
            }
            if(display === timerBeforeStart && timing < 0){
                clearInterval(func)
                isPlaying = true
                typingArea.classList.add('visible')
                timerBeforeStart.classList.add('hidden')
                startTimer(120, timer)
            }
        }, 1000)
    }
    }
}