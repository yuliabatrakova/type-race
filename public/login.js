window.onload = () => {
     const nameField = document.querySelector('#name-field');
     const passwordField = document.querySelector('#password-field');
     const loginButton = document.querySelector('#login');
     const wrongCredentials = document.querySelector('#wrong-credentials')

     loginButton.addEventListener('click', ev => {
         fetch('/login', {
             method:'POST',
             headers: {
                 'Content-Type': 'application/json'
             },
             body: JSON.stringify({
                 name: nameField.value,
                 password: passwordField.value
             })
         }).then(res => {
             res.json().then(body => {
                 console.log(body);
                 if(body.auth) {
                     localStorage.setItem('jwt', body.token);
                     location.replace('/game');
                 } else {
                     wrongCredentials.textContent = "wrong name or password"
                 }
             })
         }).catch(err => {
             console.log('request went wrong');
         })
     });
}